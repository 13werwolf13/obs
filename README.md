# Description

Сюда я буду заливать spec файлы и скрипты для подготовки архивов с сорцами для моего opensuse repo.

## Add repo

```bash
zypper ar --refresh obs://home:Werwolf2517 home:Werwolf2517
zypper refresh
```

### Docs by package

[Alfis docs](Alfis/opensuse_readme.md)

[btop docs](btop/opensuse_readme.md)

[fsv docs](fsv/opensuse_readme.md)

[valheim docs](valheim-server/opensuse_readme.md)

[satisfactory docs](satisfactory-server/opensuse_readme.md)

### Links

[Repo on OpenBuildService](https://build.opensuse.org/project/show/home:Werwolf2517)

[Repo on mirrors](https://download.opensuse.org/repositories/home:/Werwolf2517/)
