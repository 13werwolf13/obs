#
# spec file for package python-shell_gpt
#
# Copyright (c) 2023 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


%{?!python_module:%define python_module() python-%{**} python3-%{**}}
Name:           shell_gpt
Version:        0.9.4
Release:        0
Summary:        A command-line productivity tool powered by OpenAI GPT models, will help you accomplish your tasks faster and more efficiently

License:        MIT
URL:            https://github.com/ther1d/shell_gpt
Source:         https://files.pythonhosted.org/packages/source/s/shell-gpt/shell_gpt-%{version}.tar.gz
BuildRequires:  python-rpm-macros
BuildRequires:  %{python_module setuptools}
BuildRequires:  %{python_module hatchling}
BuildRequires:  %{python_module pip}
BuildRequires:    fdupes
Requires:         python-click
Requires:         python-distro
Requires:         python-requests
Requires:         python-rich
Requires:         python-typer
Suggests:         python-ruff
Suggests:         python-pre-commit
Requires(post):   update-alternatives
Requires(postun): update-alternatives
BuildArch:      noarch
%python_subpackages

%description
# ShellGPT
A command-line productivity tool powered by OpenAI's GPT models.

%prep
%autosetup -p1 -n shell_gpt-%{version}

%build
%pyproject_wheel

%install
%pyproject_install
%python_clone -a %{buildroot}%{_bindir}/sgpt
%python_expand %fdupes %{buildroot}%{$python_sitelib}

%post
%python_install_alternative sgpt

%postun
%python_uninstall_alternative sgpt

%files %{python_files}
%doc README.md README.md
%license LICENSE LICENSE
%python_alternative %{_bindir}/sgpt
%{python_sitelib}/shell_gpt*
%{python_sitelib}/sgpt

%changelog
