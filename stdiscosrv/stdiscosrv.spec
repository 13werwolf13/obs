#
# spec file for package stdiscosrv
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

Name:           stdiscosrv
Version:        1.25.0
Release:        0
Summary:        Syncthing discovery server
License:        GPL-3.0
URL:            https://github.com/syncthing/syncthing
Source:         %{name}-%{version}.tar.gz
Requires:       coreutils
BuildRequires:  golang-packaging
BuildRequires:  systemd-rpm-macros
BuildRequires:  sysuser-tools
Provides:       user(stdiscosrv)
%{sysusers_requires}

%description
stdiscosrv - Syncthing discovery server

%prep
%setup

%build
go build ./cmd/stdiscosrv/
%sysusers_generate_pre %{_sysusersdir}/system-user-stdiscosrv.conf stdiscosrv

%install
install -Dm 755  stdiscosrv %{buildroot}%{_bindir}/stdiscosrv
install -Dm 644  contrib/systemd/stdiscosrv.service %{buildroot}%{_unitdir}/stdiscosrv.service
install -Dm 644  contrib/systemd/stdiscosrv.tmpfiles %{buildroot}%{_tmpfilesdir}/stdiscosrv.conf
install -Dm 644  contrib/systemd/stdiscosrv.sysusers %{buildroot}%{_sysusersdir}/system-user-stdiscosrv.conf
install -Dm 644  contrib/systemd/stdiscosrv.env %{buildroot}%{_sysconfdir}/stdiscosrv.conf

%files
%license LICENSE
%doc cmd/stdiscosrv/README.md
%config(noreplace) %{_sysconfdir}/stdiscosrv.conf
%{_bindir}/stdiscosrv
%{_unitdir}/stdiscosrv.service
%{_tmpfilesdir}/stdiscosrv.conf
%{_sysusersdir}/system-user-stdiscosrv.conf

%pre -f stdiscosrv.pre
%service_add_pre stdiscosrv.service

%post
%service_add_post stdiscosrv.service
%sysusers_create %{_sysusersdir}/system-user-stdiscosrv.conf
%tmpfiles_create %{_tmpfilesdir}/stdiscosrv.conf

%preun
%service_del_preun stdiscosrv.service

%postun
%service_del_postun stdiscosrv.service

%changelog
