#!/usr/bin/env bash

set -Eeuo pipefail

VERSION='1.25.0'

clean () {
  rm -rf /tmp/stdiscosrv || true
  rm -rf /tmp/stdiscosrv-"$VERSION" || true
  rm -rf /tmp/stdiscosrv-"$VERSION".tar.gz || true
  rm -rf /tmp/obs || true
}

clean
git clone git@github.com:syncthing/syncthing.git /tmp/stdiscosrv
git clone git@gitlab.com:13werwolf13/obs /tmp/obs
cd /tmp/stdiscosrv || exit
git checkout tags/v"$VERSION"
go mod vendor
cd /tmp || exit
cp -r /tmp/obs/stdiscosrv/contrib /tmp/stdiscosrv/contrib
cp -r /tmp/stdiscosrv /tmp/stdiscosrv-"$VERSION"
tar cvzf /tmp/stdiscosrv-"$VERSION".tar.gz ./stdiscosrv-"$VERSION"
cp /tmp/stdiscosrv-"$VERSION".tar.gz ~/Загрузки/
rm -rf /tmp/stdiscosrv-"$VERSION" || exit
clean
