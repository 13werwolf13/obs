#
# spec file for package zypper-preload-upgrades
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

Name:           zypper-preload-upgrades
Version:        0.1.0
Release:        0
Summary:        timer for weekly preload updates
License:        NonFree
URL:            https://gitlab.com/13werwolf13/obs
Source:         %{name}-%{version}.tar.gz
Requires:       zypper

%description
zypper-preload-upgrades - timer for weekly preload updates

%prep
%setup

%build

%install
install -Dm 644  contrib/systemd/zypper-preload-upgrades.service %{buildroot}%{_unitdir}/zypper-preload-upgrades.service
install -Dm 644  contrib/systemd/zypper-preload-upgrades.timer %{buildroot}%{_unitdir}/zypper-preload-upgrades.timer

%files
%doc docs/readme.md
%{_unitdir}/zypper-preload-upgrades.service
%{_unitdir}/zypper-preload-upgrades.timer

%pre
%service_add_pre zypper-preload-upgrades.timer

%post
%service_add_post zypper-preload-upgrades.timer

%preun
%service_del_preun zypper-preload-upgrades.timer

%postun
%service_del_postun zypper-preload-upgrades.timer

%changelog
