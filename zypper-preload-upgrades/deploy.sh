#!/usr/bin/env bash

cd /tmp || exit
git clone git@gitlab.com:13werwolf13/obs
cd ./obs || exit
cp -r ./zypper-preload-upgrades ./zypper-preload-upgrades-0.1.0
tar cvzf ./zypper-preload-upgrades-0.1.0.tar.gz ./zypper-preload-upgrades-0.1.0
cp ./zypper-preload-upgrades-0.1.0.tar.gz ~/Загрузки/
rm -rf /tmp/obs
