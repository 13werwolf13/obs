# Valheim dedicated server

## install

```bash
zypper ar --refresh obs://home:Werwolf2517 home:Werwolf2517
zypper --gpg-auto-import-keys refresh
zypper install -l -y zypper-preload-upgrades
```

## configure

open systemd unit editor
```bash
systemctl edit zypper-preload-upgrades.timer
```
and replace `OnCalendar` from week to blank and then (next string) replace with the value you need
```
OnCalendar=
OnCalendar=dayli
```

## run

```bash
systemctl enable zypper-preload-upgrades.timer
```
