## Installation:
```bash
zypper ar --refresh obs://home:Werwolf2517 home:Werwolf2517
zypper --gpg-auto-import-keys refresh
zypper install -l -y zypper-preload-upgrades
```

## Create tar
```bash
git clone git@gitlab.com:13werwolf13/obs
cd ./obs
cp -r ./zypper-preload-upgrades ./zypper-preload-upgrades-0.1.0
tar cvzf ./zypper-preload-upgrades-0.1.0.tar.gz ./zypper-preload-upgrades-0.1.0
```
