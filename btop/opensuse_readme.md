## Installation:
```bash
zypper ar --refresh obs://home:Werwolf2517 home:Werwolf2517
zypper --gpg-auto-import-keys refresh
zypper install -y btop
```

## Build
```bash
zypper install -y -t pattern devel_basis
zypper install -y git gcc-c++
git clone git@github.com:aristocratos/btop.git
cd ./btop
git checkout $VERSION
make
```

## Create tar
Just download it from [github btop releases page](https://github.com/aristocratos/btop/releases/)
