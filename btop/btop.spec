#
# spec file for package btop
#
# Copyright (c) 2018 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

Name:           btop
Version:        1.2.13
Release:        0
Summary:        A monitor of resources
License:        Apache License 2.0
URL:            https://github.com/aristocratos/btop
Source:         %{url}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz
BuildRequires:  gcc11
BuildRequires:  gcc11-c++


%description
Resource monitor that shows usage and stats for processor, memory, disks, network and processes.
C++ version and continuation of bashtop and bpytop.

%prep
%autosetup

%build
export CC="/usr/bin/gcc-10"
export CXX="/usr/bin/g++-10"
export PLATFORM_LC=linux
%make_build

%install
%make_install PREFIX=%{_prefix}

%files
%{_bindir}/%{name}
%{_datadir}/%{name}/
%license LICENSE
%doc README.md

%changelog
