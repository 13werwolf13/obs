#!/usr/bin/env bash

set -Eeuo pipefail

VERSION='1.25.0'

clean () {
  rm -rf /tmp/strelaysrv || true
  rm -rf /tmp/strelaysrv-"$VERSION" || true
  rm -rf /tmp/strelaysrv-"$VERSION".tar.gz || true
  rm -rf /tmp/obs || true
}

clean
git clone git@github.com:syncthing/syncthing.git /tmp/strelaysrv
git clone git@gitlab.com:13werwolf13/obs /tmp/obs
cd /tmp/strelaysrv || exit
git checkout tags/v"$VERSION"
go mod vendor
cd /tmp || exit
cp -r /tmp/obs/strelaysrv/contrib /tmp/strelaysrv/contrib
cp -r /tmp/strelaysrv /tmp/strelaysrv-"$VERSION"
tar cvzf /tmp/strelaysrv-"$VERSION".tar.gz ./strelaysrv-"$VERSION"
cp /tmp/strelaysrv-"$VERSION".tar.gz ~/Загрузки/
rm -rf /tmp/strelaysrv-"$VERSION" || exit
clean
