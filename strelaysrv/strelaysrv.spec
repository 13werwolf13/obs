#
# spec file for package strelaysrv
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

Name:           strelaysrv
Version:        1.25.0
Release:        0
Summary:        Syncthing relay server
License:        GPL-3.0
URL:            https://github.com/syncthing/syncthing
Source:         %{name}-%{version}.tar.gz
Requires:       coreutils
BuildRequires:  golang-packaging
BuildRequires:  systemd-rpm-macros
BuildRequires:  sysuser-tools
Provides:       user(strelaysrv)
%{sysusers_requires}

%description
strelaysrv - Syncthing relay server

%prep
%setup

%build
go build ./cmd/strelaysrv/
%sysusers_generate_pre %{_sysusersdir}/system-user-strelaysrv.conf strelaysrv

%install
install -Dm 755  strelaysrv %{buildroot}%{_bindir}/strelaysrv
install -Dm 644  contrib/systemd/strelaysrv.service %{buildroot}%{_unitdir}/strelaysrv.service
install -Dm 644  contrib/systemd/strelaysrv.tmpfiles %{buildroot}%{_tmpfilesdir}/strelaysrv.conf
install -Dm 644  contrib/systemd/strelaysrv.sysusers %{buildroot}%{_sysusersdir}/system-user-strelaysrv.conf
install -Dm 644  contrib/systemd/strelaysrv.env %{buildroot}%{_sysconfdir}/strelaysrv.conf

%files
%license LICENSE
%doc cmd/strelaysrv/README.md
%config(noreplace) %{_sysconfdir}/strelaysrv.conf
%{_bindir}/strelaysrv
%{_unitdir}/strelaysrv.service
%{_tmpfilesdir}/strelaysrv.conf
%{_sysusersdir}/system-user-strelaysrv.conf

%pre -f strelaysrv.pre
%service_add_pre strelaysrv.service

%post
%service_add_post strelaysrv.service
%sysusers_create %{_sysusersdir}/system-user-strelaysrv.conf
%tmpfiles_create %{_tmpfilesdir}/strelaysrv.conf

%preun
%service_del_preun strelaysrv.service

%postun
%service_del_postun strelaysrv.service

%changelog
