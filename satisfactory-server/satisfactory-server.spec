#
# spec file for package satisfactory-server
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

Name:           satisfactory-server
Version:        0.1.0
Release:        0
Summary:        dedicated server for satisfactory
License:        NonFree
URL:            https://gitlab.com/13werwolf13/obs
Source:         %{name}-%{version}.tar.gz
ExclusiveArch:  %{ix86} x86_64
BuildArch:      %{ix86} x86_64
Requires:       steamcmd
BuildRequires:  systemd-rpm-macros
BuildRequires:  sysuser-tools
Provides:       user(satisfactory-server)
%{sysusers_requires}

%description
satisfactory-server - dedicated server for satisfactory

%prep
%setup

%build
%sysusers_generate_pre %{_sysusersdir}/system-user-satisfactory-server.conf satisfactory-server

%install
install -Dm 644  contrib/systemd/satisfactory-server.service %{buildroot}%{_unitdir}/satisfactory-server.service
install -Dm 644  contrib/systemd/satisfactory-server.tmpfiles %{buildroot}%{_tmpfilesdir}/satisfactory-server.conf
install -Dm 644  contrib/systemd/satisfactory-server.sysusers %{buildroot}%{_sysusersdir}/system-user-satisfactory-server.conf
install -Dm 644  contrib/systemd/satisfactory.env %{buildroot}%{_sysconfdir}/satisfactory.conf

%files
%doc docs/readme.md
%{_unitdir}/satisfactory-server.service
%{_tmpfilesdir}/satisfactory-server.conf
%{_sysusersdir}/system-user-satisfactory-server.conf
%config(noreplace) %{_sysconfdir}/satisfactory.conf

%pre -f satisfactory-server.pre
%service_add_pre satisfactory-server.service

%post
%service_add_post satisfactory-server.service
%sysusers_create %{_sysusersdir}/system-user-satisfactory-server.conf
%tmpfiles_create %{_tmpfilesdir}/satisfactory-server.conf

%preun
%service_del_preun satisfactory-server.service

%postun
%service_del_postun satisfactory-server.service

%changelog
