#!/usr/bin/env bash

cd /tmp || exit
git clone git@gitlab.com:13werwolf13/obs
cd ./obs || exit
cp -r ./satisfactory-server ./satisfactory-server-0.1.0
tar cvzf ./satisfactory-server-0.1.0.tar.gz ./satisfactory-server-0.1.0
cp ./satisfactory-server-0.1.0.tar.gz ~/Загрузки/
rm -rf /tmp/obs
