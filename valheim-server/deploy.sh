#!/usr/bin/env bash

cd /tmp || exit
git clone git@gitlab.com:13werwolf13/obs
cd ./obs || exit
cp -r ./valheim-server ./valheim-server-0.1.0
tar cvzf ./valheim-server-0.1.0.tar.gz ./valheim-server-0.1.0
cp ./valheim-server-0.1.0.tar.gz ~/Загрузки/
rm -rf /tmp/obs
