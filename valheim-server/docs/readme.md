# Valheim dedicated server

## install

```bash
zypper ar --refresh obs://home:Werwolf2517 home:Werwolf2517
zypper --gpg-auto-import-keys refresh
zypper install -l -y valheim-server
```

## configure

if you use non anonymous steam login you must run this as `valheim` user before start service:
```bash
steamcmd +login $YOUR_STEAM_LOGIN_HERE
```
and login with your password and 2fa

config with server name, world name, port and password here:
`/etc/valheim.conf`

## run

```bash
systemctl enable --now valheim-server.service
```
