## Installation:
```bash
zypper ar --refresh obs://home:Werwolf2517 home:Werwolf2517
zypper --gpg-auto-import-keys refresh
zypper install -l -y valheim-server
```

## Create tar
```bash
git clone git@gitlab.com:13werwolf13/obs
cd ./obs
cp -r ./valheim-server ./valheim-server-0.1.0
tar cvzf ./valheim-server-0.1.0.tar.gz ./valheim-server-0.1.0
```
