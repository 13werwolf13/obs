#
# spec file for package valheim-server
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

Name:           valheim-server
Version:        0.1.0
Release:        0
Summary:        dedicated server for valheim
License:        NonFree
URL:            https://gitlab.com/13werwolf13/obs
Source:         %{name}-%{version}.tar.gz
ExclusiveArch:  %{ix86} x86_64
BuildArch:      %{ix86} x86_64
Requires:       steamcmd
BuildRequires:  systemd-rpm-macros
BuildRequires:  sysuser-tools
Provides:       user(valheim-server)
%{sysusers_requires}

%description
valheim-server - dedicated server for valheim

%prep
%setup

%build
%sysusers_generate_pre %{_sysusersdir}/system-user-valheim-server.conf valheim-server

%install
install -Dm 644  contrib/systemd/valheim-server.service %{buildroot}%{_unitdir}/valheim-server.service
install -Dm 644  contrib/systemd/valheim-server.tmpfiles %{buildroot}%{_tmpfilesdir}/valheim-server.conf
install -Dm 644  contrib/systemd/valheim-server.sysusers %{buildroot}%{_sysusersdir}/system-user-valheim-server.conf
install -Dm 644  contrib/systemd/valheim.env %{buildroot}%{_sysconfdir}/valheim.conf

%files
%doc docs/readme.md
%{_unitdir}/valheim-server.service
%{_tmpfilesdir}/valheim-server.conf
%{_sysusersdir}/system-user-valheim-server.conf
%config(noreplace) %{_sysconfdir}/valheim.conf

%pre -f valheim-server.pre
%service_add_pre valheim-server.service

%post
%service_add_post valheim-server.service
%sysusers_create %{_sysusersdir}/system-user-valheim-server.conf
%tmpfiles_create %{_tmpfilesdir}/valheim-server.conf

%preun
%service_del_preun valheim-server.service

%postun
%service_del_postun valheim-server.service

%changelog
