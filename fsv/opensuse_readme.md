## Installation:
```bash
zypper ar --refresh obs://home:Werwolf2517 home:Werwolf2517
zypper --gpg-auto-import-keys refresh
zypper install -y fsv
```

## Build
```bash
zypper install -y -t pattern devel_basis
zypper install -y git Mesa-libGL1-32bit autoconf automake freeglut-devel gtk2-devel gtkglarea2-devel libGLU1-32bit libgtkgl-2_0-1 libtool fdupes
git clone https://github.com/mcuelenaere/fsv.git
cd ./fsv
./configure
make
```

## Create tar
```bash
git clone https://github.com/mcuelenaere/fsv.git ./fsv-0.9.1
tar cvzf ./fsv-0.9.1.tar.gz ./fsv-0.9.1
```
