#
# spec file for package fsv
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


%define _lto_cflags %{nil}
%define maj_ver 0.9
%define min_ver 1
%define git_repo https://github.com/mcuelenaere/fsv
Name:           fsv
Version:        %{maj_ver}.%{min_ver}
Release:        0
Summary:        3D File System Visualizer
#Copyright: LGPL
License:        LGPL-2.1-only
Group:          X11/Utilities
URL:            http://fox.mit.edu/skunk/soft/fsv/
Source:         %{git_repo}/archive/refs/tags/fsv-%{maj_ver}-%{min_ver}.tar.gz#/%{name}-%{version}.tar.gz
BuildRequires:  Mesa-libGL1-32bit
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  freeglut-devel
BuildRequires:  gtk2-devel
BuildRequires:  gtkglarea2-devel
BuildRequires:  libGLU1-32bit
BuildRequires:  libgtkgl-2_0-1
BuildRequires:  libtool
BuildRequires:  fdupes

ExclusiveArch:  x86_64

%description
fsv (pronounced effessvee) is a file system visualizer in cyberspace. It
lays out files and directories in three dimensions, geometrically
representing the file system hierarchy to allow visual overview and
analysis. fsv can visualize a modest home directory, a workstation's hard
drive, or any arbitrarily large collection of files, limited only by the
host computer's memory and hardware constraints.

%prep
%autosetup -n %{name}-%{name}-%{maj_ver}-%{min_ver}

%build
export CFLAGS="%{optflags}"
NOCONFIGURE=1 ./autogen.sh
%configure \
 --prefix=%{_prefix} \
 --with-doc-dir=%{_docdir}/%{name}
%make_build

%install
%make_install \
 datadir=%{buildroot}%{_datadir}/%{name}
mkdir -p %{buildroot}%{_sysconfdir}/X11/wmconfig
install -m 0644 fsv.wmconfig %{buildroot}%{_sysconfdir}/X11/wmconfig/fsv
%fdupes %{buildroot}/%{_prefix}

%files
%license COPYING
%doc README doc/*.html doc/*.png
%{_bindir}/fsv
%dir %{_sysconfdir}/X11/wmconfig
%config %{_sysconfdir}/X11/wmconfig/fsv

%changelog
