#!/usr/bin/env bash

TAG="0.5.1"

rm -rf /tmp/watchmatetest
mkdir -p /tmp/watchmatetest/{tar,build}
cp ./watchmate.spec /tmp/watchmatetest/build/
cd /tmp/watchmatetest/tar || exit

git clone git@github.com:azymohliad/watchmate.git ./watchmate-"$TAG"
cd ./watchmate-"$TAG" || exit
git checkout v"$TAG"
cargo vendor

### add vendor to cargo config
mkdir ./.cargo
echo -e "\n" >> ./.cargo/config.toml
cat <<EOF >> ./.cargo/config.toml
[source.crates-io]
replace-with = "vendored-sources"

[source."git+https://github.com/Relm4/Relm4"]
git = "https://github.com/Relm4/Relm4"
replace-with = "vendored-sources"

[source."git+https://github.com/pop-os/mpris2-zbus"]
git = "https://github.com/pop-os/mpris2-zbus"
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "vendor"
EOF

echo -e "\n" >> ./.cargo/config.toml


cd ../ || exit
chmod -R 755 ./watchmate-"$TAG"
tar cvzf ./watchmate-"$TAG".tar.gz ./watchmate-"$TAG"
mv ./watchmate-"$TAG".tar.gz ~/Загрузки/

rm -rf /tmp/watchmatetest
