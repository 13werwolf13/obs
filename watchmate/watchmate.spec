#
# spec file for package watchmate
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

Name:           watchmate
Version:        0.5.1
Release:        0
Summary:        Companion app for InfiniTime-powered PineTime smart watch.
License:        GPL-3.0
URL:            https://github.com/azymohliad/watchmate
Source:         %{name}-%{version}.tar.gz
Requires:       coreutils
#BuildRequires:  rust-packaging
BuildRequires:  cargo-packaging
BuildRequires:  gcc
BuildRequires:  glib2-devel
BuildRequires:  libadwaita-devel
BuildRequires:  libopenssl-devel
BuildRequires:  dbus-1-devel

%description
watchmate - Companion app for InfiniTime-powered PineTime smart watch.

%prep
%setup

%build
#%%cargo_build
cargo build --offline --release --frozen

%install
#%%cargo_install
install -Dm 755  target/release/watchmate %{buildroot}%{_bindir}/watchmate
install -Dm 644 assets/io.gitlab.azymohliad.WatchMate.gschema.xml -t %{buildroot}%{_datadir}/glib-2.0/schemas

%files
%license LICENSE
%doc README.md
%{_bindir}/watchmate
%{_datadir}/glib-2.0/schemas/io.gitlab.azymohliad.WatchMate.gschema.xml

%pre
glib-compile-schemas %{_datadir}/glib-2.0/schemas

%changelog
