## Installation:
```bash
zypper ar --refresh obs://home:Werwolf2517 home:Werwolf2517
zypper --gpg-auto-import-keys refresh
zypper install -y watchmate
```

## Build
```bash
zypper install -y -t pattern devel_basis
zypper in -y rust-packaging git
git clone git@github.com:Revertron/watchmate.git
cd ./watchmate
git checkout $VERSION
cargo build --release --no-default-features
```

## Create tar with deps
```bash
./deploy.sh
```
