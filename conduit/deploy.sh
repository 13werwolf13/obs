#!/usr/bin/env bash

TAG="0.6.0"

git clone git@gitlab.com:famedly/conduit.git /tmp/conduit-"$TAG"
git clone git@gitlab.com:13werwolf13/obs /tmp/obs
cd /tmp/conduit-"$TAG" || exit
git checkout v"$TAG"
cargo vendor

### add vendor to cargo config
mkdir -p ./.cargo
echo -e "\n" >> ./.cargo/config.toml
cat <<EOF >> ./.cargo/config.toml
[source.crates-io]
replace-with = "vendored-sources"

[source."git+https://github.com/ruma/ruma?rev=3bd58e3c899457c2d55c45268dcb8a65ae682d54"]
git = "https://github.com/ruma/ruma"
rev = "3bd58e3c899457c2d55c45268dcb8a65ae682d54"
replace-with = "vendored-sources"

[source."git+https://github.com/timokoesters/heed.git?rev=f6f825da7fb2c758867e05ad973ef800a6fe1d5d"]
git = "https://github.com/timokoesters/heed.git"
rev = "f6f825da7fb2c758867e05ad973ef800a6fe1d5d"
replace-with = "vendored-sources"

[source."git+https://github.com/timokoesters/reqwest?rev=57b7cf4feb921573dfafad7d34b9ac6e44ead0bd"]
git = "https://github.com/timokoesters/reqwest"
rev = "57b7cf4feb921573dfafad7d34b9ac6e44ead0bd"
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "vendor"
EOF

echo -e "\n" >> ./.cargo/config.toml

cp -r /tmp/obs/conduit/contrib /tmp/conduit-"$TAG"/contrib

cd ../ || exit
chmod -R 755 ./conduit-"$TAG"
tar czf ./conduit-"$TAG".tar.gz ./conduit-"$TAG"
mv ./conduit-"$TAG".tar.gz ~/Загрузки/

rm -rf /tmp/conduit
