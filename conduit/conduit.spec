#
# spec file for package conduit
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

Name:           conduit
Version:        0.6.0
Release:        0
Summary:        Conduit Matrix Server
License:        Apache-2.0
URL:            https://gitlab.com/famedly/conduit
Source:         %{name}-%{version}.tar.gz
Requires:       coreutils
#BuildRequires:  rust-packaging
BuildRequires:  cargo-packaging
BuildRequires:  systemd-rpm-macros
BuildRequires:  glib2-devel
BuildRequires:  sysuser-tools
Provides:       user(conduit)
%{sysusers_requires}

%description
Conduit Matrix Server

%prep
%setup

%build
#%%cargo_build
cargo build --offline --release --frozen

%sysusers_generate_pre %{_sysusersdir}/system-user-conduit.conf conduit

%install
#%%cargo_install
install -Dm 755  target/release/conduit %{buildroot}%{_bindir}/conduit
install -Dm 644  contrib/systemd/conduit.service %{buildroot}%{_unitdir}/conduit.service
install -Dm 644  conduit-example.toml %{buildroot}%{_tmpfilesdir}/conduit.toml
install -Dm 644  contrib/systemd/conduit.sysusers %{buildroot}%{_sysusersdir}/system-user-conduit.conf

%files
%license LICENSE
%{_bindir}/conduit
%{_unitdir}/conduit.service
%{_tmpfilesdir}/conduit.toml
%{_sysusersdir}/system-user-conduit.conf

%pre -f conduit.pre
%service_add_pre conduit.service

%post
%service_add_post conduit.service
%sysusers_create %{_sysusersdir}/system-user-conduit.conf
%tmpfiles_create %{_tmpfilesdir}/conduit.conf

%preun
%service_del_preun conduit.service

%postun
%service_del_postun conduit.service

%changelog
