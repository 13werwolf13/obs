#
# spec file for package Alfis
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

Name:           Alfis
Version:        0.8.4
Release:        0
Summary:        Alternative Free Identity System
License:        GPL-3.0
URL:            https://github.com/Revertron/Alfis
Source:         %{name}-%{version}.tar.gz
Requires:       coreutils
BuildRequires:  rust-packaging
#BuildRequires:  cargo-packaging
BuildRequires:  systemd-rpm-macros
BuildRequires:  glib2-devel
BuildRequires:  sysuser-tools
BuildRequires:  libsoup2-devel
#BuildRequires:  libsoup-devel
#BuildRequires:  webkit2gtk3-devel
BuildRequires:  webkit2gtk4-devel
BuildRequires:  webkit2gtk3-soup2-devel
BuildRequires:  wayland-devel
Provides:       user(alfis)
%{sysusers_requires}

%description
Alfis - Alternative Free Identity System

%prep
%setup

%build
#%%cargo_build
cargo build --offline --release --frozen

%sysusers_generate_pre %{_sysusersdir}/system-user-alfis.conf alfis

%install
#%%cargo_install
install -Dm 755  target/release/alfis %{buildroot}%{_bindir}/alfis
install -Dm 644  contrib/systemd/alfis-default-config.service %{buildroot}%{_unitdir}/alfis-default-config.service
install -Dm 644  contrib/systemd/alfis.service %{buildroot}%{_unitdir}/alfis.service
install -Dm 644  contrib/systemd/alfis.tmpfiles %{buildroot}%{_tmpfilesdir}/alfis.conf
install -Dm 644  contrib/systemd/alfis.sysusers %{buildroot}%{_sysusersdir}/system-user-alfis.conf
install -Dm 644  contrib/name.alfis.Alfis.desktop %{buildroot}%{_datadir}/applications/%{name}.desktop
install -Dm 644  img/logo/alfis_logo.svg %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/alfis.svg

%files
%license LICENSE
%doc README.md docs/about_ru.md docs/faq_ru.md
%{_bindir}/alfis
%{_unitdir}/alfis-default-config.service
%{_unitdir}/alfis.service
%{_tmpfilesdir}/alfis.conf
%{_sysusersdir}/system-user-alfis.conf
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/scalable/apps/alfis.svg

%pre -f alfis.pre
%service_add_pre alfis.service alfis-default-config.service

%post
%service_add_post alfis.service alfis-default-config.service
%sysusers_create %{_sysusersdir}/system-user-alfis.conf
%tmpfiles_create %{_tmpfilesdir}/alfis.conf

%preun
%service_del_preun alfis.service alfis-default-config.service

%postun
%service_del_postun alfis.service alfis-default-config.service

%changelog
