#!/usr/bin/env bash

TAG="0.8.4"

rm -rf /tmp/alfistest
mkdir -p /tmp/alfistest/{tar,build}
cp ./Alfis.spec /tmp/alfistest/build/
cd /tmp/alfistest/tar || exit

git clone git@github.com:Revertron/Alfis.git ./Alfis-"$TAG"
cd ./Alfis-"$TAG" || exit
git checkout v"$TAG"
#cargo install cargo-lock2rpmprovides
#cargo lock2rpmprovides
#cargo update
cargo vendor

### add vendor to cargo config
echo -e "\n" >> ./.cargo/config.toml
cat <<EOF >> ./.cargo/config.toml
[source.crates-io]
replace-with = "vendored-sources"

[source."git+https://github.com/Boscop/web-view"]
git = "https://github.com/Boscop/web-view"
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "vendor"
EOF

echo -e "\n" >> ./.cargo/config.toml

#sed -i '/ExecStartPost/d' ./contrib/systemd/alfis-default-config.service

### config permission patch
#sed -i 's/Group/User/' ./contrib/systemd/alfis-default-config.service

cd ../ || exit
chmod -R 755 ./Alfis-"$TAG"
tar cvzf ./Alfis-"$TAG".tar.gz ./Alfis-"$TAG"
mv ./Alfis-"$TAG".tar.gz ~/Загрузки/

rm -rf /tmp/alfistest
